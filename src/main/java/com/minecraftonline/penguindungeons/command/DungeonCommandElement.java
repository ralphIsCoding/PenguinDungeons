package com.minecraftonline.penguindungeons.command;

import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.dungeon.Dungeon;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.ArgumentParseException;
import org.spongepowered.api.command.args.CommandArgs;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.util.StartsWithPredicate;

import javax.annotation.Nullable;
import java.util.List;
import java.util.stream.Collectors;

public class DungeonCommandElement extends CommandElement {

    public DungeonCommandElement(@Nullable Text key) {
        super(key);
    }

    @Nullable
    @Override
    protected Object parseValue(CommandSource source, CommandArgs args) throws ArgumentParseException {
        final String name = args.next();
        final Dungeon dungeon = PenguinDungeons.getDungeons().get(name);
        if (dungeon == null) {
            throw new ArgumentParseException(Text.of("No dungeon with name '" + name + "'"), name, 0);
        }
        return dungeon;
    }

    @Override
    public List<String> complete(CommandSource src, CommandArgs args, CommandContext context) {
        final String current = args.nextIfPresent().orElse("");
        return PenguinDungeons.getDungeons().keySet().stream()
                .filter(new StartsWithPredicate(current))
                .collect(Collectors.toList());
    }
}

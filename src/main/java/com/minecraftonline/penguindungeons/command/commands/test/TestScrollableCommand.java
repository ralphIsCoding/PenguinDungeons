package com.minecraftonline.penguindungeons.command.commands.test;

import com.minecraftonline.penguindungeons.command.AbstractCommand;
import com.minecraftonline.penguindungeons.gui.inventory.ScrollableInventoryList;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.stream.Collectors;

public class TestScrollableCommand extends AbstractCommand {

    public TestScrollableCommand() {
        setExecutor((src, args) -> {
            if (!(src instanceof Player)) {
                throw new CommandException(Text.of("You must be a player to execute this command!"));
            }
            final Player player = (Player) src;
            ScrollableInventoryList scrollableInventoryList = new ScrollableInventoryList(
                    Text.of(TextColors.DARK_GRAY, "Scrollable Inventory Test"),
                    Sponge.getRegistry().getAllOf(ItemType.class).stream().map(ItemStack::of).collect(Collectors.toList()),
                    ((p, itemStack, index, shift) -> {
                        p.sendMessage(Text.of("Item: " + itemStack));
                        p.sendMessage(Text.of("Index: " + index));
                        p.sendMessage(Text.of("Shifting: " + shift));
                    }));
            player.openInventory(scrollableInventoryList.create());
            return CommandResult.success();
        });
    }
}

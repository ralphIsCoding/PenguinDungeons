package com.minecraftonline.penguindungeons.command.commands;

import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.command.AbstractCommand;

public class DungeonCommand extends AbstractCommand {

    public static final String BASE_PERMISSION = PenguinDungeons.BASE_PERMISSION + "dungeon.";

    public DungeonCommand() {
        addChild(new CreateDungeonCommand(), "create");
        addChild(new DeleteDungeonCommand(), "delete");
        addChild(new SpawnDungeonCommand(), "spawn");
        addChild(new ListDungeonsCommand(), "list");
        addChild(new ListDungeonNodesCommand(), "listnodes");
    }

}

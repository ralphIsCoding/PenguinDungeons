package com.minecraftonline.penguindungeons.command.commands;

import com.minecraftonline.penguindungeons.command.AbstractCommand;

public class PDCommandRoot extends AbstractCommand {
    public PDCommandRoot() {
        addChild(new BrowseCommand(), "browse");
        addChild(new DungeonCommand(), "dungeon");
        addChild(new WandCommand(), "wand");
        addChild(new SaveCommand(), "save");
        //addChild(new TestCommand(), "test"); // Uncomment for testing purposes.
    }
}

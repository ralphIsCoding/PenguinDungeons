package com.minecraftonline.penguindungeons.registry;

import com.minecraftonline.penguindungeons.util.ResourceKey;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class PDRegistry<T> implements PDReadableRegistry<T> {

    protected final Map<ResourceKey, T> map = new HashMap<>();
    protected final Collection<PDReadableRegistry<? extends T>> fallbackRegistries = new ArrayList<>();

    public void register(ResourceKey key, T value) {
        this.map.put(key, value);
    }

    /**
     * Registers a fallback registry, so that if no value can be
     * found for a given ResourceKey, the fallback registry is checked.
     *
     * @param fallbackRegistry FallbackRegistry
     */
    public void registerFallback(PDReadableRegistry<? extends T> fallbackRegistry) {
        this.fallbackRegistries.add(fallbackRegistry);
    }

    public Optional<T> find(final ResourceKey key) {
        T value = this.map.get(key);
        if (value != null) {
            return Optional.of(value);
        }
        for (PDReadableRegistry<? extends T> fallbackRegistry : this.fallbackRegistries) {
            Optional<? extends T> foundValue = fallbackRegistry.find(key);
            if (foundValue.isPresent()) {
                return Optional.of(foundValue.get());
            }
        }
        return Optional.empty();
    }
}

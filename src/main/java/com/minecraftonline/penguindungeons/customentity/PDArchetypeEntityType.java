package com.minecraftonline.penguindungeons.customentity;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.DataView;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.EntityArchetype;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.serializer.TextSerializers;
import org.spongepowered.api.world.World;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PDArchetypeEntityType implements PDEntityType {

    private final EntityArchetype entityArchetype;

    public PDArchetypeEntityType(final EntityArchetype entityArchetype) {
        this.entityArchetype = entityArchetype;
    }

    @Override
    public String getPermission() {
        return PenguinDungeons.BASE_PERMISSION + "entity.spawner";
    }

    @Override
    public EntityType getType() {
        return this.entityArchetype.getType();
    }

    @Override
    public ResourceKey getId() {
        throw new UnsupportedOperationException("This object should never be stored!");
    }

    @Override
    public ItemStack getSpawnEgg() {
        List<DataView> manipulators = this.entityArchetype.getEntityData().getViewList(
                DataQuery.of("ForgeData", "SpongeData", "CustomManipulators")).orElse(new ArrayList<DataView>());
        for (DataView manipulator : manipulators)
        {
            Optional<String> id = manipulator.getString(DataQuery.of("ManipulatorId"));
            if (!id.isPresent() || !id.get().equals("penguindungeons:pd_entity_type")) continue;
            Optional<String> keyString = manipulator.getString(DataQuery.of("ManipulatorData", "PDEntityType"));
            if (keyString.isPresent())
            {
                Optional<ResourceKey> key = ResourceKey.tryResolve(keyString.get());
                if (key.isPresent())
                {
                    Optional<CustomEntityType> pdEntityType = CustomEntityTypes.getById(key.get());
                    if (pdEntityType.isPresent())
                    {
                        // use normal PD egg if this is for a PD entity type
                        return pdEntityType.get().getSpawnEgg();
                    }
                }
                break;
            }
        }
        ItemStack.Builder builder = ItemStack.builder()
                .itemType(ItemTypes.SPAWN_EGG);
        builder.add(Keys.SPAWNABLE_ENTITY_TYPE, this.entityArchetype.getType());
        this.entityArchetype.getEntityData().getString(DataQuery.of("CustomName")).map(TextSerializers.LEGACY_FORMATTING_CODE::deserialize)
                .ifPresent(name -> builder.add(Keys.DISPLAY_NAME, name));
        return builder.build();
    }

    @Override
    public Spawnable createEntity(World world, Vector3d pos) {
        throw new UnsupportedOperationException("This object should only be for display purposes!");
    }

    @Override
    public Optional<EntityArchetype> getEntityArchetype() {
        return Optional.of(entityArchetype);
    }
}

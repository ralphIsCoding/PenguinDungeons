package com.minecraftonline.penguindungeons.customentity.dragon;

import javax.annotation.Nullable;

import net.minecraft.entity.boss.dragon.phase.PhaseDying;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;

public class CustomPhaseDying extends PhaseDying implements CustomIPhase {
    private Vec3d targetLocation;
    private int time;
    private CustomDragon dragon;

    public CustomPhaseDying(CustomDragon dragonIn) {
        super(dragonIn);
        this.dragon = dragonIn;
    }

    /**
     * Gives the phase a chance to update its status.
     * Called by dragon's onLivingUpdate. Only used when !worldObj.isRemote.
     */
    public void doLocalUpdate() {
        ++this.time;
        if (this.targetLocation == null) {
            BlockPos blockpos = this.dragon.podium;
            this.targetLocation = new Vec3d((double)blockpos.getX(), (double)blockpos.getY(), (double)blockpos.getZ());
        }

        double d0 = this.targetLocation.squareDistanceTo(this.dragon.posX, this.dragon.posY, this.dragon.posZ);
        if (d0 >= 100.0D && d0 <= 22500.0D && !this.dragon.collidedHorizontally && !this.dragon.collidedVertically) {
            this.dragon.setHealth(1.0F);
        } else {
            this.dragon.setHealth(0.0F);
        }

    }

    /**
     * Called when this phase is set to active
     */
    public void initPhase() {
        this.targetLocation = null;
        this.time = 0;
    }

    /**
     * Returns the maximum amount dragon may rise or fall during this phase
     */
    public float getMaxRiseOrFall() {
        return 3.0F;
    }

    /**
     * Returns the location the dragon is flying toward
     */
    @Nullable
    public Vec3d getTargetLocation() {
        return this.targetLocation;
    }

    @Override
    public CustomPhaseList<? extends CustomIPhase> getCustomType() {
        return CustomPhaseList.DYING;
    }

}

package com.minecraftonline.penguindungeons.customentity.zombie;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.monster.ZombieVillager;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.ai.DelegatingToMCAI;
import com.minecraftonline.penguindungeons.customentity.HasEffects;
import com.minecraftonline.penguindungeons.customentity.LaserGuardian;
import com.minecraftonline.penguindungeons.customentity.FollowFromDistance;
import com.minecraftonline.penguindungeons.customentity.ShootLasers;
import com.minecraftonline.penguindungeons.data.PenguinDungeonAITaskTypes;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAIZombieAttack;
import net.minecraft.entity.monster.EntityZombieVillager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.MobEffects;
import net.minecraft.potion.PotionEffect;

public class LaserZombie extends ZombieType<ZombieVillager> implements HasEffects {

    public LaserZombie(ResourceKey key) {
        super(key);
    }

    @Override
    public EntityType getType() {
        return EntityTypes.ZOMBIE_VILLAGER;
    }

    public Text getDisplayName() {
        return Text.of(TextColors.RED, "Doctor Dead");
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A villager zombie that shoots laser"));
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onLoad(Entity entity) {
        super.onLoad(entity);
        if (!(entity instanceof ZombieVillager)) {
            throw new IllegalArgumentException("Expected a ZombieVillager to be given to LaserZombie to load, but got: " + entity);
        }
        ZombieVillager zombie = (ZombieVillager) entity;

        Goal<Agent> normalGoals = zombie.getGoal(GoalTypes.NORMAL).get();
        // Remove regular attack.
        normalGoals.getTasks().stream().filter(aiTask -> aiTask instanceof EntityAIZombieAttack)
                .forEach(task -> normalGoals.removeTask((AITask<? extends Agent>) task));

        normalGoals.addTask(1, new FollowFromDistance.FollowDistance(zombie));
        normalGoals.addTask(2, new LaserAttack(zombie, this));


        Goal<Agent> targetGoals = zombie.getGoal(GoalTypes.TARGET).get();
        // remove regular target goal
        targetGoals.getTasks().stream().filter(aiTask -> aiTask instanceof EntityAINearestAttackableTarget)
                .forEach(task -> targetGoals.removeTask((AITask<? extends Agent>) task));
        // add new target goal
        targetGoals.addTask(1, (AITask<? extends Agent>) new EntityAINearestAttackableTarget<EntityPlayer>(
                (EntityCreature) entity, EntityPlayer.class, 10, true, false, new LaserGuardian.GuardianTargetSelector((EntityCreature) entity)));
    }

    @Override
    protected ZombieVillager makeZombie(World world, Vector3d blockPos) {
        ZombieVillager zombie = super.makeZombie(world, blockPos);
        EntityZombieVillager mcZombie = (EntityZombieVillager) zombie;
        mcZombie.setProfession(1); // white robe
        return zombie;
    }

    public static class LaserAttack extends DelegatingToMCAI<ZombieVillager> {

        public LaserAttack(ZombieVillager zombie, HasEffects shooter) {
            super(PenguinDungeonAITaskTypes.SHOOT_LASERS, new ShootLasers<EntityZombieVillager>(shooter, zombie));
        }
    }

    @Override
    public List<PotionEffect> getEffects() {
        return Arrays.asList(
                new PotionEffect(MobEffects.INSTANT_DAMAGE),
                new PotionEffect(MobEffects.SLOWNESS, TICKS_PER_SECOND*5, 0));
    }
}

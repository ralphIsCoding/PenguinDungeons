package com.minecraftonline.penguindungeons.customentity.zombie;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.Creature;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.customentity.AbstractCustomEntity;
import com.minecraftonline.penguindungeons.customentity.EntityAIHurtByNonPD;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.data.loot.OwnerLootData;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.ai.EntityAIMoveThroughVillage;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.player.EntityPlayer;

public abstract class ZombieType<T extends Creature> extends AbstractCustomEntity {

    public ZombieType(ResourceKey key) {
        super(key);
    }

    public abstract Text getDisplayName();

    @Override
    public EntityType getType() {
        return EntityTypes.ZOMBIE;
    }

    @Override
    public ItemStack getSpawnEgg() {
        return ItemStack.builder().itemType(ItemTypes.SPAWN_EGG)
                .add(Keys.SPAWNABLE_ENTITY_TYPE, getType())
                .add(Keys.DISPLAY_NAME, getDisplayName())
                .add(Keys.ITEM_LORE, getDescription())
                .itemData(new PDEntityTypeData(getId()))
                .build();
    }

    @Override
    public Spawnable createEntity(World world, Vector3d pos) {
        return Spawnable.of(makeZombie(world, pos), this::onLoad);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onLoad(Entity entity)
    {
        if (!(entity instanceof Creature)) {
            throw new IllegalArgumentException("Expected a creature to be given to ZombieType to load, but got: " + entity);
        }
        Creature zombie = (Creature) entity;

        // remove task for moving through village
        Goal<Agent> normalGoals = zombie.getGoal(GoalTypes.NORMAL).get();
        normalGoals.getTasks().stream().filter(aiTask -> aiTask instanceof EntityAIMoveThroughVillage)
        .forEach(task -> normalGoals.removeTask((AITask<? extends Agent>) task));

        // remove task for attacking villager/irongolem/player
        // remove revenge task to stop them from attacking eachother
        Goal<Agent> targetGoals = zombie.getGoal(GoalTypes.TARGET).get();
        targetGoals.getTasks().stream().filter(aiTask -> aiTask instanceof EntityAINearestAttackableTarget
                                                      || aiTask instanceof EntityAIHurtByTarget)
        .forEach(task -> targetGoals.removeTask((AITask<? extends Agent>) task));

        // add back attacking players
        targetGoals.addTask(1, new EntityAIHurtByNonPD.TargetNonPDAttackers((Creature) entity, true, true));
        targetGoals.addTask(2, (AITask<? extends Agent>) new EntityAINearestAttackableTarget<EntityPlayer>((EntityCreature) entity, EntityPlayer.class, true));
    }

    protected T makeZombie(World world, Vector3d blockPos) {
        @SuppressWarnings("unchecked")
        T zombie = (T) world.createEntity(getType(), blockPos);
        zombie.offer(Keys.DISPLAY_NAME, getDisplayName());
        zombie.offer(new PDEntityTypeData(getId()));
        zombie.offer(new OwnerLootData(ownerLoot()));

        // entity should despawn
        zombie.offer(Keys.PERSISTS, false);

        applyEquipment(zombie);
        applyLootTable(zombie);

        return zombie;
    }

    public boolean ownerLoot()
    {
        return true;
    }

}

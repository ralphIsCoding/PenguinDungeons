package com.minecraftonline.penguindungeons.customentity;

import org.spongepowered.api.entity.living.Creature;

import com.minecraftonline.penguindungeons.ai.DelegatingToMCAI;
import com.minecraftonline.penguindungeons.data.PenguinDungeonAITaskTypes;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAIBase;

public class FollowFromDistance<T extends EntityCreature> extends EntityAIBase {

    /** The entity the AI instance has been applied to */
    private final EntityLiving entityHost;
    private EntityLivingBase attackTarget;
    private int seeTime;
    private final float maxDistance;
    private final double entityMoveSpeed;

    public FollowFromDistance(Creature attacker) {
        this(attacker, 1.25D, 10.0F);
    }

    public FollowFromDistance(Creature attacker, double movespeed, float maxDistanceIn) {
       if (!(attacker instanceof EntityLivingBase)) {
          throw new IllegalArgumentException("ArrowAttackGoal requires Mob implements RangedAttackMob");
       } else {
          this.entityHost = (EntityLiving)attacker;
          this.maxDistance = maxDistanceIn * maxDistanceIn;
          this.entityMoveSpeed = movespeed;
          this.setMutexBits(3);
       }
    }

    /**
     * Returns whether the EntityAIBase should begin execution.
     */
    public boolean shouldExecute() {
       EntityLivingBase entitylivingbase = this.entityHost.getAttackTarget();
       if (entitylivingbase == null || !entitylivingbase.isEntityAlive()) {
          return false;
       } else {
          this.attackTarget = entitylivingbase;
          return true;
       }
    }

    /**
     * Returns whether an in-progress EntityAIBase should continue executing
     */
    public boolean shouldContinueExecuting() {
       return this.shouldExecute() || !this.entityHost.getNavigator().noPath();
    }

    /**
     * Reset the task's internal state. Called when this task is interrupted by another one
     */
    public void resetTask() {
       this.attackTarget = null;
       this.seeTime = 0;
    }

    /**
     * Keep ticking a continuous task that has already been started
     */
    public void updateTask() {
       double d0 = this.entityHost.getDistanceSq(this.attackTarget.posX, this.attackTarget.getEntityBoundingBox().minY, this.attackTarget.posZ);
       boolean flag = this.entityHost.getEntitySenses().canSee(this.attackTarget);
       if (flag) {
          ++this.seeTime;
       } else {
          this.seeTime = 0;
       }

       if (d0 <= (double)this.maxDistance && this.seeTime >= 20) {
          this.entityHost.getNavigator().clearPath();
       } else {
          this.entityHost.getNavigator().tryMoveToEntityLiving(this.attackTarget, this.entityMoveSpeed);
       }

       this.entityHost.getLookHelper().setLookPositionWithEntity(this.attackTarget, 30.0F, 30.0F);
       if (!flag) {
          return;
       }

    }

    public static class FollowDistance extends DelegatingToMCAI<Creature> {

        public FollowDistance(Creature creature) {
            super(PenguinDungeonAITaskTypes.FOLLOW_DISTANCE, new FollowFromDistance<EntityCreature>(creature));
        }
    }
}
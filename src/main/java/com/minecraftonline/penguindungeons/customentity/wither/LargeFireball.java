package com.minecraftonline.penguindungeons.customentity.wither;

import com.minecraftonline.penguindungeons.ai.AIUtil.ProjectileForImpact;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityLargeFireball;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;

public class LargeFireball extends EntityLargeFireball {
    private ProjectileForImpact forImpact;

    public LargeFireball(World world, EntityLivingBase shooter, double accelX, double accelY, double accelZ, ProjectileForImpact forImpact) {
        super(world, shooter, accelX, accelY, accelZ);
        this.setLocationAndAngles(shooter.posX, shooter.posY + (shooter.height / 2.0F), shooter.posZ, shooter.rotationYaw, shooter.rotationPitch);
        this.forImpact = forImpact;
    }

    @Override
    protected void onImpact(RayTraceResult result) {
        this.forImpact.forImpact(result, this);
    }
}

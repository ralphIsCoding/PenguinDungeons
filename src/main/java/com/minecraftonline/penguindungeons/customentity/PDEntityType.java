package com.minecraftonline.penguindungeons.customentity;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.DataView;
import org.spongepowered.api.entity.EntityArchetype;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.world.World;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public interface PDEntityType {

    ResourceKey getId();

    default String getPermission() {
        return PenguinDungeons.BASE_PERMISSION + "entity." + getId().formattedWith(".");
    }

    public EntityType getType();

    ItemStack getSpawnEgg();

    /**
     * Creates an entity of this type.
     * @param world World to spawn.
     * @param pos Position to spawn
     * @return Entity, using this type.
     */
    Spawnable createEntity(World world, Vector3d pos);

    default Optional<EntityArchetype> getEntityArchetype() {
        DataView entityTag = DataContainer.createNew();
        List<DataView> manipulators = new ArrayList<DataView>();
        DataView manipulator = DataContainer.createNew();
        manipulator.set(DataQuery.of("ContentVersion"), 2);
        manipulator.set(DataQuery.of("ManipulatorId"), "penguindungeons:pd_entity_type");
        DataView manipulatorData = DataContainer.createNew();
        manipulatorData.set(DataQuery.of("ContentVersion"), 1);
        manipulatorData.set(DataQuery.of("PDEntityType"), getId().asString());
        manipulator.set(DataQuery.of("ManipulatorData"), manipulatorData);
        manipulators.add(manipulator);
        entityTag.set(DataQuery.of("ForgeData", "SpongeData", "CustomManipulators"), manipulators);
        return Optional.of(EntityArchetype.builder()
                .type(getType())
                .entityData(entityTag)
                .build());
    }
}

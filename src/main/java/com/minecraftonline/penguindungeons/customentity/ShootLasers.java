package com.minecraftonline.penguindungeons.customentity;


import org.spongepowered.api.entity.living.Creature;
import org.spongepowered.api.entity.living.Living;
import org.spongepowered.api.entity.living.monster.Guardian;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.monster.EntityGuardian;
import net.minecraft.init.MobEffects;
import net.minecraft.potion.PotionEffect;

public class ShootLasers<T extends EntityCreature> extends EntityAIBase {

    /** The entity the AI instance has been applied to */
    private final HasEffects shooter;
    private final Living host;
    private final EntityLiving entityHost;
    private LaserGuardian guardian;
    private boolean teleport = false;

    public ShootLasers(Creature attacker) {
        this((HasEffects) attacker, attacker);
    }

    public ShootLasers(HasEffects shooter, Creature attacker) {
       if (!(shooter instanceof HasEffects)) {
          throw new IllegalArgumentException("ShootLasers requires Shooter implements CanShootLasers");
       } else {
           this.shooter = (HasEffects) shooter;
       }
       if (!(attacker instanceof Living)) {
           throw new IllegalArgumentException("ShootLasers requires Mob implements EntityLiving");
       } else {
           this.host = attacker;
           this.entityHost = (EntityLiving) attacker;
       }
    }

    /**
     * Returns whether the EntityAIBase should begin execution.
     */
    public boolean shouldExecute() {
       return this.teleport || this.guardian == null;
    }

    /**
     * Returns whether an in-progress EntityAIBase should continue executing
     */
    public boolean shouldContinueExecuting() {
       return this.shouldExecute();
    }

    /**
     * Reset the task's internal state. Called when this task is interrupted by another one
     */
    public void resetTask() {
    }

    /**
     * Keep ticking a continuous task that has already been started
     */
    public void updateTask() {
       if (this.guardian == null)
       {
           // create laser turret (guardian)
           this.guardian = new LaserGuardian(entityHost.world, shooter, entityHost);
           this.guardian.setPosition(entityHost.posX, entityHost.posY + 2, entityHost.posZ);
           this.guardian.addPotionEffect(new PotionEffect(MobEffects.INVISIBILITY, 999999, 0, false, false));
           this.guardian.setSilent(true);
           this.guardian.setNoGravity(true);
           this.guardian.setCustomNameTag("\u00a7cLaser Turret");
           this.guardian.enablePersistence();
           entityHost.world.spawnEntity((EntityGuardian) this.guardian);
           if (!teleport) {
               this.host.addPassenger((Guardian) this.guardian);
           }
       }
       else if (teleport)
       {
           this.guardian.setPosition(entityHost.posX, entityHost.posY + 2, entityHost.posZ);
       }

    }
}
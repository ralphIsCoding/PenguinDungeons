package com.minecraftonline.penguindungeons.data.loot;

import org.spongepowered.api.data.manipulator.immutable.common.AbstractImmutableBooleanData;

import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;

public class ImmutableOwnerLootData extends AbstractImmutableBooleanData<ImmutableOwnerLootData, OwnerLootData>
{

	public ImmutableOwnerLootData(boolean value)
	{
		super(PenguinDungeonKeys.OWNER_LOOT, value, false);
	}
	
    @Override
    public OwnerLootData asMutable()
    {
        return new OwnerLootData(this.getValue());
    }

    @Override
    public int getContentVersion()
    {
        return 1;
    }

}

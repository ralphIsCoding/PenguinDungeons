package com.minecraftonline.penguindungeons.data;

import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.ai.ShulkerDelegatingToMCAI;
import com.minecraftonline.penguindungeons.customentity.EntityAIHurtByNonPD;
import com.minecraftonline.penguindungeons.customentity.FollowFromDistance;
import com.minecraftonline.penguindungeons.customentity.StraightProjectileAttack;
import com.minecraftonline.penguindungeons.customentity.ShieldAttack;
import com.minecraftonline.penguindungeons.customentity.golem.ExplodingSnowGolem;
import com.minecraftonline.penguindungeons.customentity.golem.IceSnowGolem;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.ExplodingShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.PaladinShulker;
import com.minecraftonline.penguindungeons.customentity.zombie.LaserZombie;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.ai.task.AITaskType;
import org.spongepowered.api.entity.ai.task.AbstractAITask;
import org.spongepowered.api.entity.living.Agent;

public class PenguinDungeonAITaskTypes {

    public static AITaskType SHULKER_SNIPE;

    public static AITaskType SHULKER_EXPLODE;

    public static AITaskType SHULKER_SEARCH_FOR_EXPLODABLES;

    public static AITaskType SHULKER_FIREY_ATTACK;

    public static AITaskType SHULKER_BLINDING_ATTACK;

    public static AITaskType SHULKER_EFFECT_ATTACK;

    public static AITaskType CHANGE_COLOUR;

    public static AITaskType SHULKER_INVULNERABLE_WHEN_CLOSED;

    public static AITaskType SHULKER_INVULNERABLE_BULLETS;

    public static AITaskType STRAIGHT_PROJECTILE_RANGED_ATTACK;

    public static AITaskType SNOW_GOLEM_MELEE_ATTACK;

    public static AITaskType SNOW_GOLEM_EXPLODE;

    public static AITaskType SNOW_GOLEM_SEARCH_FOR_EXPLODABLES;

    public static AITaskType SHOOT_LASERS;

    public static AITaskType FOLLOW_DISTANCE;

    public static AITaskType SHIELD_ATTACK;

    public static AITaskType ATTACK_BACK_NON_PD;

    public static void register() {
        SHULKER_EXPLODE = register( "shulker_explode", "Shulker Explode", ExplodingShulker.StartExploding.class);

        SHULKER_SEARCH_FOR_EXPLODABLES = register("shulker_search_for_explodables", "Shulker search for explodables", ExplodingShulker.FindExplodableTarget.class);

        SHULKER_FIREY_ATTACK = registerShulkerDelegating("shulker_firey_attack", "Shulker Firey Attack");

        SHULKER_EFFECT_ATTACK = registerShulkerDelegating("shulker_effect_attack", "Shulker Effect Attack");

        CHANGE_COLOUR = registerShulkerDelegating("shulker_change_colour", "Shulker Change Colour");

        SHULKER_INVULNERABLE_WHEN_CLOSED = register("shulker_invulnerable_when_closed", "Shulker Invulnerable When Closed", PaladinShulker.InvulnerableWhenClosed.class);

        SHULKER_INVULNERABLE_BULLETS = registerShulkerDelegating("shulker_invulnerable_bullets", "Shulker Invulnerable Bullets");

        STRAIGHT_PROJECTILE_RANGED_ATTACK = register("straight_shulker_bullet_attack", "Snow Golem Ranged Attack", StraightProjectileAttack.RangedAttack.class);

        SNOW_GOLEM_MELEE_ATTACK = register("snow_golem_melee_attack", "Snow Golem Melee Attack", IceSnowGolem.MeleeAttack.class);

        SNOW_GOLEM_EXPLODE = register( "snow_golem_explode", "Shulker Explode", ExplodingSnowGolem.StartExploding.class);

        SNOW_GOLEM_SEARCH_FOR_EXPLODABLES = register("snow_golem_search_for_explodables", "Snow Golem search for explodables", ExplodingSnowGolem.FindExplodableTarget.class);

        SHOOT_LASERS = register( "laser_attack", "Laser Attack", LaserZombie.LaserAttack.class);

        FOLLOW_DISTANCE = register( "follow_distance", "Follow from a distance", FollowFromDistance.FollowDistance.class);

        SHIELD_ATTACK = register( "shield_attack", "Follow from a distance", ShieldAttack.AttackWithShield.class);

        ATTACK_BACK_NON_PD = register( "attack_back_non_pd", "Attack other living entities that hurt this and are not from Penguin Dungeons", EntityAIHurtByNonPD.TargetNonPDAttackers.class);
    }

    private static AITaskType register(final String id, final String name, Class<? extends AbstractAITask<? extends Agent>> aiClass) {
        return Sponge.getRegistry().registerAITaskType(PenguinDungeons.getInstance(), id, name, aiClass);
    }

    private static AITaskType registerShulkerDelegating(final String id, final String name) {
        return register(id, name, ShulkerDelegatingToMCAI.class);
    }
}

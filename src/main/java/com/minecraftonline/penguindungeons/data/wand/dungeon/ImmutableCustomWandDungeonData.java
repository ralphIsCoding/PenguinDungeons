package com.minecraftonline.penguindungeons.data.wand.dungeon;

import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.manipulator.immutable.common.AbstractImmutableSingleData;
import org.spongepowered.api.data.value.immutable.ImmutableValue;

public class ImmutableCustomWandDungeonData extends AbstractImmutableSingleData<String, ImmutableCustomWandDungeonData, CustomWandDungeonData> {

    public ImmutableCustomWandDungeonData(String value) {
        super(PenguinDungeonKeys.PD_WAND_DUNGEON, value);
    }

    @Override
    protected ImmutableValue<?> getValueGetter() {
        return Sponge.getRegistry().getValueFactory()
                .createValue(PenguinDungeonKeys.PD_WAND_DUNGEON, getValue())
                .asImmutable();
    }

    @Override
    public CustomWandDungeonData asMutable() {
        return new CustomWandDungeonData(this.getValue());
    }

    @Override
    public int getContentVersion() {
        return 1;
    }
}

package com.minecraftonline.penguindungeons.data;

import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeDataBuilder;
import com.minecraftonline.penguindungeons.data.customentity.ImmutablePDEntityTypeData;
import com.minecraftonline.penguindungeons.data.loot.ImmutableOwnerLootData;
import com.minecraftonline.penguindungeons.data.loot.OwnerLootData;
import com.minecraftonline.penguindungeons.data.loot.OwnerLootDataBuilder;
import com.minecraftonline.penguindungeons.data.wand.CustomWandData;
import com.minecraftonline.penguindungeons.data.wand.CustomWandDataBuilder;
import com.minecraftonline.penguindungeons.data.wand.ImmutableCustomWandData;
import com.minecraftonline.penguindungeons.data.wand.dungeon.CustomWandDungeonData;
import com.minecraftonline.penguindungeons.data.wand.dungeon.CustomWandDungeonDataBuilder;
import com.minecraftonline.penguindungeons.data.wand.dungeon.ImmutableCustomWandDungeonData;
import com.minecraftonline.penguindungeons.util.PDTypeTokens;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import static org.spongepowered.api.data.DataQuery.of;

import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.DataRegistration;
import org.spongepowered.api.data.key.Key;
import org.spongepowered.api.data.value.mutable.Value;
import org.spongepowered.api.util.TypeTokens;

public class PenguinDungeonKeys {

    public static Key<Value<ResourceKey>> PD_ENTITY_TYPE = Key.builder()
            .type(PDTypeTokens.RESOURCE_KEY_VALUE_TOKEN)
            .id("pd_entity_type")
            .name("PenguinDungeons Entity Type")
            .query(DataQuery.of("PDEntityType"))
            .build();

    public static Key<Value<Boolean>> IS_PD_WAND = Key.builder()
            .type(TypeTokens.BOOLEAN_VALUE_TOKEN)
            .id("is_pd_wand")
            .name("Is Penguin Dungeons Wand")
            .query(DataQuery.of("IsPDWand"))
            .build();

    public static Key<Value<String>> PD_WAND_DUNGEON = Key.builder()
            .type(TypeTokens.STRING_VALUE_TOKEN)
            .id("pd_wand_dungeon")
            .name("Penguin Dungeons Wands Dungeon")
            .query(DataQuery.of("PDWandDungeon"))
            .build();

    public static Key<Value<Boolean>> OWNER_LOOT = Key.builder()
            .type(TypeTokens.BOOLEAN_VALUE_TOKEN)
            .id("ownerloot")
            .name("Owner Loot")
            .query(of("OwnerLoot"))
            .build();

    public static void register() {
        DataRegistration.builder()
                .dataClass(PDEntityTypeData.class)
                .immutableClass(ImmutablePDEntityTypeData.class)
                .builder(new PDEntityTypeDataBuilder())
                .id("pd_entity_type")
                .name("PenguinDungeons Entity Type")
                .build();

        DataRegistration.builder()
                .dataClass(CustomWandData.class)
                .immutableClass(ImmutableCustomWandData.class)
                .builder(new CustomWandDataBuilder())
                .id("is_pd_wand")
                .name("Is Penguin Dungeons Wand")
                .build();

        DataRegistration.builder()
                .dataClass(CustomWandDungeonData.class)
                .immutableClass(ImmutableCustomWandDungeonData.class)
                .builder(new CustomWandDungeonDataBuilder())
                .id("pd_wand_dungeon")
                .name("Penguin Dungeons Wands Dungeon")
                .build();

        DataRegistration.builder()
                .dataClass(OwnerLootData.class)
                .immutableClass(ImmutableOwnerLootData.class)
                .builder(new OwnerLootDataBuilder())
                .id("owner_loot")
                .name("Owner Loot")
                .build();
    }
}

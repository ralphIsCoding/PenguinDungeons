package com.minecraftonline.penguindungeons.ai;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityShulkerBullet;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;

public class ShulkerBulletInvulnerable extends EntityShulkerBullet {

    public ShulkerBulletInvulnerable(World worldIn) {
        super(worldIn);
    }

    public ShulkerBulletInvulnerable(World worldIn, EntityLivingBase ownerIn, Entity targetIn, EnumFacing.Axis p_i46772_4_) {
        super(worldIn, ownerIn, targetIn, p_i46772_4_);
    }

    @Override
    public boolean attackEntityFrom(DamageSource source, float amount) {
    	// bullet can not be destroyed from attack
        return false;
     }
}

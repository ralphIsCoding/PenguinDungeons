package com.minecraftonline.penguindungeons.dungeon;

import com.minecraftonline.penguindungeons.customentity.PDEntityType;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Optional;
import java.util.Random;

public interface SpawnOption {

    /**
     * Gets the next CustomEntityType to be spawned.
     * @param rand Random number generator to be used if desired.
     * @return The next CustomEntityType, if available
     */
    Optional<PDEntityType> next(Random rand);

    /**
     * Checks whether the a custom mob should be
     * spawned in this location.
     * This could depend on whether there is already
     * a nearby mob of the same type.
     * @param loc Location
     * @return Whether a mob should be spawned in the given location.
     */
    boolean shouldSpawn(Location<World> loc);
}

package com.minecraftonline.penguindungeons.mixin;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.EnumCreatureAttribute;
import net.minecraft.world.World;

import java.util.Optional;

import org.spongepowered.api.entity.living.Living;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.minecraftonline.penguindungeons.customentity.CustomEntityTypes;
import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.util.ResourceKey;

@Mixin(EntityLivingBase.class)
public abstract class EntityLivingBaseMixin extends Entity {

    public EntityLivingBaseMixin(World worldIn) {
        super(worldIn);
    }

    @Inject(method = "getCreatureAttribute", at = @At("HEAD"), cancellable = true)
    public void onGetCreatureAttribute(CallbackInfoReturnable<EnumCreatureAttribute> cir) {
        Living living = (Living) this;
        final Optional<ResourceKey> id = living.get(PenguinDungeonKeys.PD_ENTITY_TYPE);
        if (id.isPresent() && id.get().equals(CustomEntityTypes.ZOMBIE_DOG.getId()))
        {
            // zombie dogs are undead
            cir.setReturnValue(EnumCreatureAttribute.UNDEAD);
        }
    }
}
